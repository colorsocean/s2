package main

import (
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"
	"os"

	"bitbucket.org/colorsocean/rtool"
	"bitbucket.org/colorsocean/s2"
)

func main() {
	stor, err := s2.New(s2.Options{})
	if err != nil {
		panic(err)
	}

	if false {
		onofile, err := os.Open("./ono.jpg")
		if err != nil {
			panic(err)
		}
		defer onofile.Close()

		s2f, err := stor.StoreReader(s2.StoreReader{
			R:    onofile,
			Name: "onofile.jpg",
			MIME: "image/jpeg",
		})
		if err != nil {
			panic(err)
		}

		{
			data, err := json.MarshalIndent(s2f, "", "  ")
			if err != nil {
				panic(err)
			}
			fmt.Println(string(data))
		}
		fmt.Println(string(s2f.ID()))
		fmt.Println(string(s2f.HeapURLWithHash()))

	}

	heapBase := "/static/heap/"
	http.Handle(heapBase, http.StripPrefix(heapBase, stor.ServeHeapWithHash()))
	http.Handle("/upload", http.HandlerFunc(func(w http.ResponseWriter, q *http.Request) {
		var req UploadReq
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}
		_, err = stor.StoreMultipart(s2.StoreMultipart{
			File: req.File,
		})
		if err != nil {
			panic(err)
		}
		http.Redirect(w, q, "/", http.StatusSeeOther)
	}))
	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, q *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprintf(w, `<pre>
		<form method="post" action="/upload" enctype="multipart/form-data">
		<input name="file" type="file"/>
		<input type="submit" />
		</form>`)
		files, err := stor.Files()
		if err != nil {
			panic(err)
		}
		for _, file := range files {
			fmt.Fprintf(w, `<pre><a href="%s%s">%s</a>`, heapBase, file.HeapURLWithHash(), file.Name)
		}
	}))

	fmt.Println(http.ListenAndServe(":9001", nil))
}

type UploadReq struct {
	File *multipart.FileHeader
}
