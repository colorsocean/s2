package s2

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/boltdb/bolt"
)

func (this *File) Update() error {
	return this.stor.db.Update(func(tx *bolt.Tx) error {
		v, err := gobEncode(this)
		if err != nil {
			return err
		}
		heap := tx.Bucket(bucketHeap)
		return heap.Put([]byte(this.id), v)
	})
}

func (this *File) HeapURLWithHash() string {
	return fmt.Sprintf("%s-%s", this.MD5String(), this.ID())
}

func (this *File) FilePath() string {
	return filepath.Join(this.stor.filesDir, this.id)
}

func (this *File) Open() (*os.File, error) {
	return os.Open(this.FilePath())
}

func (this *File) OpenRW() (*os.File, error) {
	return os.OpenFile(this.FilePath(), os.O_RDWR, this.stor.ops.DefaultPerm)
}

func (this *File) OpenTrunc() (*os.File, error) {
	return os.OpenFile(this.FilePath(),
		os.O_RDWR|os.O_CREATE|os.O_TRUNC|os.O_SYNC,
		this.stor.ops.DefaultPerm)
}

func (this *File) Serve(w http.ResponseWriter, dl bool) error {
	w.Header().Set("Content-Type", this.MIME)

	attachment := "attachment"
	if !dl {
		attachment = ""
	}

	w.Header().Set("Content-Disposition", fmt.Sprint(attachment, "; filename=", this.Name))

	f, err := this.Open()
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(w, f)
	if err != nil {
		return err
	}

	return nil
}

type File struct {
	id string

	Name         string
	MIME         string
	detectedMIME string

	size int

	imageWidth  int
	imageHeight int

	Meta []byte

	uploaded time.Time
	md5      [16]byte

	DownloadByDefault bool

	stor *Storage
}

func (this *File) IsImage() bool {
	return this.imageWidth != 0 && this.imageHeight != 0
}

func (this *File) ID() string {
	return this.id
}

func (this *File) Size() int {
	return this.size
}

func (this *File) ImageWidth() int {
	return this.imageWidth
}

func (this *File) ImageHeight() int {
	return this.imageHeight
}

func (this *File) Uploaded() time.Time {
	return this.uploaded
}

func (this *File) MD5() [16]byte {
	return this.md5
}

func (this *File) MD5String() string {
	return fmt.Sprintf("%x", string(this.md5[:]))
}

func (this *File) GobWrite(w io.Writer) (err error) {
	encoder := gob.NewEncoder(w)
	encode := func(v interface{}) {
		if err == nil {
			err = encoder.Encode(v)
		}
	}
	encode(this.id)
	encode(this.Name)
	encode(this.MIME)
	encode(this.detectedMIME)
	encode(this.size)
	encode(this.imageWidth)
	encode(this.imageHeight)
	encode(this.Meta)
	encode(this.uploaded)
	encode(this.md5)
	encode(this.DownloadByDefault)
	return
}

func (this *File) GobEncode() ([]byte, error) {
	w := bytes.NewBuffer([]byte{})
	err := this.GobWrite(w)
	if err != nil {
		return nil, err
	}
	return w.Bytes(), nil
}

func (this *File) GobRead(r io.Reader) (err error) {
	decoder := gob.NewDecoder(r)
	decode := func(v interface{}) {
		if err == nil {
			err = decoder.Decode(v)
		}
	}
	decode(&this.id)
	decode(&this.Name)
	decode(&this.MIME)
	decode(&this.detectedMIME)
	decode(&this.size)
	decode(&this.imageWidth)
	decode(&this.imageHeight)
	decode(&this.Meta)
	decode(&this.uploaded)
	decode(&this.md5)
	decode(&this.DownloadByDefault)
	return
}

func (this *File) GobDecode(buf []byte) error {
	r := bytes.NewReader(buf)
	err := this.GobRead(r)
	if err != nil {
		return err
	}
	return nil
}

func (this *File) Info() *FileInfo {
	return &FileInfo{
		ID:          this.ID(),
		MIME:        this.MIME,
		ImageWidth:  this.ImageWidth(),
		ImageHeight: this.ImageHeight(),
	}
}

type FileInfo struct {
	ID string

	MIME string

	ImageWidth  int `json:",omitempty"`
	ImageHeight int `json:",omitempty"`
}
