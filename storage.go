package s2

import (
	"bytes"
	"crypto/md5"
	"encoding/gob"
	"errors"
	"fmt"
	"image"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/colorsocean/iox"

	"github.com/boltdb/bolt"
	"github.com/kardianos/osext"
	"github.com/satori/go.uuid"

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

var (
	ErrFileNotFound = errors.New("File not found")

	bucketHeap = []byte("heap")
)

type Options struct {
	Directory   string
	DefaultPerm os.FileMode
}

type Storage struct {
	ops Options

	db       *bolt.DB
	filesDir string
	dbPath   string
}

func New(ops Options) (stor *Storage, err error) {
	if ops.DefaultPerm == 0 {
		ops.DefaultPerm = 0770
	}

	if ops.Directory == "" {
		ops.Directory, err = osext.Executable()
		if err != nil {
			return
		}
		if runtime.GOOS == "windows" {
			ops.Directory = strings.TrimSuffix(ops.Directory, ".exe")
		}
		ops.Directory += ".storage"
	}

	stor = &Storage{
		ops: ops,
	}

	stor.dbPath = filepath.Join(stor.ops.Directory, "db")
	stor.filesDir = filepath.Join(stor.ops.Directory, "files")

	err = os.MkdirAll(stor.ops.Directory, stor.ops.DefaultPerm|os.ModeDir)
	if err != nil {
		if !os.IsExist(err) {
			return
		}
	}

	err = os.MkdirAll(stor.filesDir, stor.ops.DefaultPerm|os.ModeDir)
	if err != nil {
		if !os.IsExist(err) {
			return
		}
	}

	stor.db, err = bolt.Open(stor.dbPath, stor.ops.DefaultPerm, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return
	}

	err = stor.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucketHeap)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return
	}

	return
}

func (this *Storage) ServeHeapWithHash() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, q *http.Request) {
		path := strings.Trim(q.URL.Path, "/")
		fmt.Println(path)
		spl := strings.Split(path, "-")
		if len(spl) == 2 {
			file, err := this.File(spl[1])
			if err != nil {
				if err == ErrFileNotFound {
					http.NotFound(w, q)
					return
				} else {
					panic(err)
				}
			}
			if spl[0] == file.MD5String() {
				var dl bool = file.DownloadByDefault
				if _, ok := q.URL.Query()["dl"]; ok {
					dl = true
				}

				err = file.Serve(w, dl)
				if err != nil {
					if err == ErrFileNotFound {
						http.NotFound(w, q)
						return
					} else {
						panic(err)
					}
				}
				return
			}
		}
		http.NotFound(w, q)
	})
}

func (this *Storage) ServeHeap() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, q *http.Request) {
		path := strings.Trim(q.URL.Path, "/")
		file, err := this.File(path)
		if err != nil {
			if err == ErrFileNotFound {
				http.NotFound(w, q)
				return
			} else {
				panic(err)
			}
		}

		var dl bool = file.DownloadByDefault
		if _, ok := q.URL.Query()["dl"]; ok {
			dl = true
		}

		err = file.Serve(w, dl)
		if err != nil {
			if err == ErrFileNotFound {
				http.NotFound(w, q)
				return
			} else {
				panic(err)
			}
		}

		http.NotFound(w, q)
	})
}

type StoreMultipart struct {
	File *multipart.FileHeader

	DownloadByDefault bool
}

func (this *Storage) StoreMultipart(o StoreMultipart) (*File, error) {
	f, err := o.File.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()

	file, err := this.Store(Store{
		R:                 f,
		Name:              o.File.Filename,
		MIME:              o.File.Header.Get("Content-Type"),
		DownloadByDefault: o.DownloadByDefault,
	})
	if err != nil {
		return nil, err
	}

	return file, nil
}

type Store struct {
	R   io.Reader
	WFn func(io.Writer) error

	SuccessFn func(file *File, r io.ReadSeeker) error

	Name              string
	MIME              string
	DownloadByDefault bool
}

func IsImage(mime string) bool {
	return strings.HasPrefix(mime, "image/")
}

// Low level store function
func (this *Storage) Store(store Store) (*File, error) {
	file := &File{
		uploaded: time.Now(),
		id:       uuidND(),
		stor:     this,
	}

	if store.Name != "" {
		file.Name = store.Name
	} else {
		file.Name = file.id
	}

	file.MIME = store.MIME
	file.DownloadByDefault = store.DownloadByDefault

	hash := md5.New()
	counter := iox.NewCountingWriter(ioutil.Discard)

	f, err := os.OpenFile(file.FilePath(),
		os.O_RDWR|os.O_CREATE|os.O_SYNC,
		this.ops.DefaultPerm)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	defer f.Sync()

	w := io.MultiWriter(f, hash, counter)

	if store.WFn != nil {
		err := store.WFn(w)
		if err != nil {
			return nil, err
		}
	} else {
		_, err := io.Copy(w, store.R)
		if err != nil {
			return nil, err
		}
	}

	//> Save hash
	copy(file.md5[:], hash.Sum(nil))

	//> Save size
	file.size = counter.WriteCount()

	{ //> Detect MIME
		_, err := f.Seek(0, 0)
		if err != nil {
			return nil, err
		}

		var buf [512]byte

		if _, err := f.Read(buf[:]); err != nil {
			// todo: warn somehow!
			log.Println("s2: ERROR: Can't open file for MIME detection:", err)
		} else {
			file.detectedMIME = http.DetectContentType(buf[:])
			if file.MIME == "" {
				file.MIME = file.detectedMIME
			} else if file.MIME != file.detectedMIME {
				log.Println("s2: WARN: MIME(", file.MIME, ") != DetectedMIME(", file.detectedMIME, ")")
			}
		}
	}

	//> Image?
	if IsImage(file.MIME) || IsImage(file.detectedMIME) {
		_, err := f.Seek(0, 0)
		if err != nil {
			return nil, err
		}

		if cfg, _, err := image.DecodeConfig(f); err != nil {
			// todo: warn somehow!
			log.Println("s2: ERROR: Can't decode image config:", err, "\n MIME =", file.MIME, "DetectedMIME =", file.detectedMIME)
		} else {
			file.imageWidth = cfg.Width
			file.imageHeight = cfg.Height
		}
		log.Println("[storage] image uploaded:", file.MIME, file.detectedMIME, file.ImageWidth(), file.ImageHeight())
	}

	err = file.Update()
	if err != nil {
		return nil, err
	}

	if store.SuccessFn != nil {
		_, err := f.Seek(0, 0)
		if err != nil {
			return nil, err
		}

		err = store.SuccessFn(file, f)
		if err != nil {
			return nil, err
		}
	}

	return file, nil
}

func (this *Storage) File(id string) (file *File, err error) {
	err = this.db.View(func(tx *bolt.Tx) error {
		heap := tx.Bucket(bucketHeap)
		fi := heap.Get([]byte(id))
		if fi == nil {
			return ErrFileNotFound
		}
		f := new(File)
		err := gobDecode(fi, f)
		if err != nil {
			return err
		}
		f.stor = this
		file = f
		return nil
	})
	if err != nil {
		return
	}
	return
}

func (this *Storage) Files() (files []*File, err error) {
	err = this.db.View(func(tx *bolt.Tx) (err error) {
		heap := tx.Bucket(bucketHeap)
		c := heap.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var file File
			err := gobDecode(v, &file)
			if err != nil {
				return err
			}
			file.stor = this
			files = append(files, &file)
		}

		return nil
	})
	if err != nil {
		return
	}
	return
}

func uuidND() string {
	u := uuid.NewV4()
	return fmt.Sprintf("%x%x%x%x%x", u[:4], u[4:6], u[6:8], u[8:10], u[10:])
}

func gobEncode(v interface{}) (data []byte, err error) {
	buf := bytes.NewBuffer(data)
	err = gob.NewEncoder(buf).Encode(v)
	if err != nil {
		return
	}
	data = buf.Bytes()
	return
}

func gobDecode(data []byte, vout interface{}) (err error) {
	return gob.NewDecoder(bytes.NewReader(data)).Decode(vout)
}
